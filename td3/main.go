package main

import (
	"fmt"
	"gitlab.utc.fr/zhenyang/ia04/td3/comsoc"
)

func main() {
	// 统一在cs_test中测试，而不是main中
	prefs := comsoc.Profile{
		{1, 2, 3},
		{2, 3, 1},
		{3, 1, 2},
		{2, 1, 3},
	}

	// 创建阈值数组的示例
	thresholds := []int{1, 2, 1, 2}

	// 测试 Majority
	majorityCount, err := comsoc.MajoritySWF(prefs)
	if err != nil {
		fmt.Println("Majority SWF Error:", err)
		return
	}
	fmt.Println("Majority Count:", majorityCount)

	majorityWinners, err := comsoc.MajoritySCF(prefs)
	if err != nil {
		fmt.Println("Majority SCF Error:", err)
		return
	}
	fmt.Println("Majority Winners:", majorityWinners)

	// 测试 Borda
	bordaCount, err := comsoc.BordaSWF(prefs)
	if err != nil {
		fmt.Println("Borda SWF Error:", err)
		return
	}
	fmt.Println("Borda Count:", bordaCount)

	bordaWinners, err := comsoc.BordaSCF(prefs)
	if err != nil {
		fmt.Println("Borda SCF Error:", err)
		return
	}
	fmt.Println("Borda Winners:", bordaWinners)

	// 测试Approval
	approvalCount, err := comsoc.ApprovalSWF(prefs, thresholds)
	if err != nil {
		fmt.Println("Approval SWF Error:", err)
		return
	}
	fmt.Println("Approval Count:", approvalCount)

	approvalWinners, err := comsoc.ApprovalSCF(prefs, thresholds)
	if err != nil {
		fmt.Println("Approval SCF Error:", err)
		return
	}
	fmt.Println("Approval Winners:", approvalWinners)

}
