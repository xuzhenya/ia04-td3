package comsoc

import (
	"errors"
	"fmt"
)

func CondorcetWinner(p Profile) (bestAlts []Alternative, err error) {
	err = checkProfile(p)
	if err != nil {
		return nil, err
	}

	win_time := make(map[Alternative]int) // Number of wins for each alternative

	for i := 1; i <= len(p[0])-1; i++ {
		for j := i + 1; j <= len(p[0]); j++ {
			count_i, count_j := 0, 0
			// 遍历Profile中每一个individu的preference，进行计票
			for _, prefs := range p {
				if isPref(Alternative(i), Alternative(j), prefs) {
					count_i++
				} else {
					count_j++
				}
			}
			// 计票结果：{i,j}, 赢的alt它的win次数+1
			if count_i > count_j {
				win_time[Alternative(i)]++
			} else {
				win_time[Alternative(j)]++
			}
		}
	}

	fmt.Println(win_time)

	// 寻找Condorcet获胜者，他的win_time[Alternative(i)]应为总参选人数-1
	for key, value := range win_time {
		if value == len(p[0])-1 {
			bestAlts = append(bestAlts, Alternative(key))
			return bestAlts, nil
		}
	}

	return nil, errors.New("Pas de vainqueur Condorcet")
}
