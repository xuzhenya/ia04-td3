package comsoc

import (
	"errors"
	"fmt"
)

func TieBreakFactory(orderedAlts []Alternative) func([]Alternative) (Alternative, error) {
	return func(candidates []Alternative) (Alternative, error) {
		if len(candidates) == 0 {
			return 0, errors.New("La liste des candidats est vide")
		}

		// 用map来查找候选人在预定义顺序中的位置
		ranking := make(map[Alternative]int)
		for i, alt := range orderedAlts {
			ranking[alt] = i
		}

		// 在候选人中查找排名最高的候选人
		winner := candidates[0]
		minRank := ranking[winner]
		for _, candidate := range candidates {
			if rank, found := ranking[candidate]; found {
				if rank < minRank {
					winner = candidate
					minRank = rank
				}
			} else {
				return 0, fmt.Errorf("Le candidat %v n'est pas dans la liste de séquence prédéfinie", candidate)
			}
		}

		return winner, nil
	}
}
