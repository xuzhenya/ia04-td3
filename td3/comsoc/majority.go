package comsoc

import (
	"errors"
)

func MajoritySWF(p Profile) (Count, error) {
	err := checkProfile(p)
	if err != nil {
		return nil, err
	}

	// 初始化一个 Count 类型的映射（map），并将其赋值给 count 变量。
	count := make(Count)
	for _, voter := range p {
		// voter[0]表示的是profile[i][0]表示第i个投票人最喜欢的那个候选人
		count[voter[0]]++
	}

	return count, nil
}

func MajoritySCF(p Profile) ([]Alternative, error) {
	count, err := MajoritySWF(p)
	if err != nil {
		return nil, err
	}

	max := 0
	var bestAlts []Alternative
	for alt, votes := range count {
		if votes > max {
			max = votes
			bestAlts = []Alternative{alt}
		} else if votes == max {
			bestAlts = append(bestAlts, alt)
		}
	}

	if len(bestAlts) > 1 {
		return bestAlts, errors.New("Il y a égalité des voix")
	}

	return bestAlts, nil
}
