package comsoc

import "errors"

func CopelandSWF(p Profile) (Count, error) {
	err := checkProfile(p)
	if err != nil {
		return nil, err
	}

	// 初始化 Copeland 计数
	copelandCount := make(Count)

	// 对每对候选者进行比较
	for _, voter := range p {
		for i := 0; i < len(voter); i++ {
			for j := i + 1; j < len(voter); j++ {
				if isPref(voter[i], voter[j], voter) {
					copelandCount[voter[i]]++
					copelandCount[voter[j]]--
				} else if isPref(voter[j], voter[i], voter) {
					copelandCount[voter[j]]++
					copelandCount[voter[i]]--
				}
			}
		}
	}

	return copelandCount, nil
}

func CopelandSCF(p Profile) ([]Alternative, error) {
	copelandCount, err := CopelandSWF(p)
	if err != nil {
		return nil, err
	}

	// 找出得分最高的候选者
	var maxScore int
	for _, score := range copelandCount {
		if score > maxScore {
			maxScore = score
		}
	}

	// 可能有多个候选者得分相同且最高
	var bestAlts []Alternative
	for alt, score := range copelandCount {
		if score == maxScore {
			bestAlts = append(bestAlts, alt)
		}
	}

	if len(bestAlts) == 0 {
		return nil, errors.New("Aucun gagnant de Copeland trouvé")
	}

	return bestAlts, nil
}
