package comsoc

// 工厂函数接受一个社会福利函数（SWF）或社会选择函数（SCF）和一个平局决策函数（TieBreak），并返回一个新的函数，该函数在调用给定的 SWF 或 SCF 后调用 TieBreak 来解决任何平局。
// SWFFactory 接受一个 swf 函数和一个 tieBreak 函数作为参数。它返回一个新的函数，该函数首先调用 swf，然后在需要时调用 tieBreak 来解决平局。新函数返回一个包含获胜选项的切片和一个错误（如果有）。
// SCFFactory 使用一个 scf 函数来确定最佳选项。如果有一个平局，它会使用 tieBreak 函数来确定最终的获胜者。新函数返回获胜选项和一个错误（如果有）。

func SWFFactory(swf func(p Profile) (Count, error), tieBreak func([]Alternative) (Alternative, error)) func(Profile) ([]Alternative, error) {
	return func(p Profile) ([]Alternative, error) {
		count, err := swf(p)
		if err != nil {
			return nil, err
		}

		maxCount := maxCount(count)
		if len(maxCount) == 1 {
			return maxCount, nil
		}

		winner, err := tieBreak(maxCount)
		if err != nil {
			return nil, err
		}
		return []Alternative{winner}, nil
	}
}

func SCFFactory(scf func(p Profile) ([]Alternative, error), tieBreak func([]Alternative) (Alternative, error)) func(Profile) (Alternative, error) {
	return func(p Profile) (Alternative, error) {
		bestAlts, err := scf(p)
		if err != nil {
			return -1, err
		}

		if len(bestAlts) == 1 {
			return bestAlts[0], nil
		}

		return tieBreak(bestAlts)
	}
}
