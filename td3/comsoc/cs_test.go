// version 2.0.0

package comsoc

import (
	"fmt"
	"testing"
)

func TestBordaSWF(t *testing.T) {
	prefs := [][]Alternative{
		{1, 2, 3},
		{1, 2, 3},
		{3, 2, 1},
	}

	res, _ := BordaSWF(prefs)

	if res[1] != 4 {
		t.Errorf("error, result for 1 should be 4, %d computed", res[1])
	}
	if res[2] != 3 {
		t.Errorf("error, result for 2 should be 3, %d computed", res[2])
	}
	if res[3] != 2 {
		t.Errorf("error, result for 3 should be 2, %d computed", res[3])
	}
}

func TestBordaSCF(t *testing.T) {
	prefs := [][]Alternative{
		{1, 2, 3},
		{1, 2, 3},
		{3, 2, 1},
	}

	res, err := BordaSCF(prefs)

	if err != nil {
		t.Error(err)
	}

	if len(res) != 1 || res[0] != 1 {
		t.Errorf("error, 1 should be the only best Alternative")
	}
}

func TestMajoritySWF(t *testing.T) {
	prefs := [][]Alternative{
		{1, 2, 3},
		{1, 2, 3},
		{3, 2, 1},
	}

	res, _ := MajoritySWF(prefs)

	if res[1] != 2 {
		t.Errorf("error, result for 1 should be 2, %d computed", res[1])
	}
	if res[2] != 0 {
		t.Errorf("error, result for 2 should be 0, %d computed", res[2])
	}
	if res[3] != 1 {
		t.Errorf("error, result for 3 should be 1, %d computed", res[3])
	}
}

func TestMajoritySCF(t *testing.T) {
	prefs := [][]Alternative{
		{1, 2, 3},
		{1, 2, 3},
		{3, 2, 1},
	}

	res, err := MajoritySCF(prefs)

	if err != nil {
		t.Error(err)
	}

	if len(res) != 1 || res[0] != 1 {
		t.Errorf("error, 1 should be the only best Alternative")
	}
}

func TestApprovalSWF(t *testing.T) {
	prefs := [][]Alternative{
		{1, 2, 3},
		{1, 3, 2},
		{2, 3, 1},
	}
	thresholds := []int{2, 1, 2}

	res, _ := ApprovalSWF(prefs, thresholds)

	if res[1] != 2 {
		t.Errorf("error, result for 1 should be 2, %d computed", res[1])
	}
	if res[2] != 2 {
		t.Errorf("error, result for 2 should be 2, %d computed", res[2])
	}
	if res[3] != 1 {
		t.Errorf("error, result for 3 should be 1, %d computed", res[3])
	}
}

func TestApprovalSCF(t *testing.T) {
	prefs := [][]Alternative{
		{1, 3, 2},
		{1, 2, 3},
		{2, 1, 3},
	}
	thresholds := []int{2, 1, 2}

	res, err := ApprovalSCF(prefs, thresholds)

	if err != nil {
		t.Error(err)
	}
	if len(res) != 1 || res[0] != 1 {
		t.Errorf("error, 1 should be the only best Alternative")
	}
}

//func TestCondorcetWinner(t *testing.T) {
//	prefs1 := [][]Alternative{
//		{1, 2, 3},
//		{1, 2, 3},
//		{3, 2, 1},
//	}
//
//	prefs2 := [][]Alternative{
//		{1, 2, 3},
//		{2, 3, 1},
//		{3, 1, 2},
//	}
//
//	res1, _ := CondorcetWinner(prefs1)
//	res2, _ := CondorcetWinner(prefs2)
//
//	if len(res1) == 0 || res1[0] != 1 {
//		t.Errorf("error, 1 should be the only best alternative for prefs1")
//	}
//	if len(res2) != 0 {
//		t.Errorf("no best alternative for prefs2")
//	}
//}

func TestTieBreakFactory(t *testing.T) {
	orderedAlts := []Alternative{1, 2, 3, 4, 5}

	tieBreak := TieBreakFactory(orderedAlts)

	// 定义一个候选人的平票情况
	tiedCandidates := []Alternative{3, 1, 5, 2}

	// 使用 TieBreak 函数决定胜者
	winner, err := tieBreak(tiedCandidates)

	fmt.Println("winner:", winner)

	if err != nil {
		t.Error(err)
	}

	// 验证胜者是否是正确的
	// 候选人1应该是胜者，因为在 orderedAlts 中他的位置最靠前
	if winner != 1 {
		t.Errorf("错误，胜者应该是1，但计算出的胜者是 %d", winner)
	}

	// 测试一个空的候选人列表，应该返回一个错误
	_, err = tieBreak([]Alternative{})
	if err != nil {
		t.Error(err)
	}

	// 测试一个包含未在 orderedAlts 中定义的候选人的列表，应该返回一个错误
	_, err = tieBreak([]Alternative{6, 7})
	if err != nil {
		t.Error(err)
	}
}

func TestCondorcetWinner(t *testing.T) {
	prefs := [][]Alternative{
		{1, 2, 3},
		{1, 3, 2},
		{3, 2, 1},
	}

	res, err := CondorcetWinner(prefs)

	if err != nil {
		t.Error(err)
	}

	// 检查 res 的长度，确保其不为空
	if len(res) == 0 {
		t.Error("Pas de vainqueur Condorcet")
		return
	}

	if len(res) != 1 {
		t.Error("Il ne devrait y avoir qu'un seul gagnant Condorcet")
	}
}

// 该测试理论结果没有胜者
func TestCondorcetWinner2(t *testing.T) {
	prefs := [][]Alternative{
		{1, 2, 3},
		{2, 3, 1},
		{3, 1, 2},
	}

	res, err := CondorcetWinner(prefs)

	if err != nil {
		t.Error(err)
	}

	// 检查 res 的长度，确保其不为空
	if len(res) == 0 {
		t.Error("Pas de vainqueur Condorcet")
		return
	}

	if len(res) != 1 {
		t.Error("Il ne devrait y avoir qu'un seul gagnant Condorcet")
	}

	if len(res) != 1 {
		t.Error(err)
	}
}

func TestCopelandSWF(t *testing.T) {
	prefs := Profile{
		{1, 2, 3, 4},
		{2, 3, 4, 1},
		{4, 3, 1, 2},
	}

	res, err := CopelandSWF(prefs)
	if err != nil {
		t.Error(err)
		return
	}

	fmt.Println("Copeland:", res)
}

func TestCopelandSCF(t *testing.T) {
	prefs := Profile{
		{1, 2, 3, 4},
		{2, 3, 4, 1},
		{4, 3, 1, 2},
	}

	res, err := CopelandSCF(prefs)
	if err != nil {
		t.Error(err)
		return
	}

	fmt.Println("Copeland winner:", res)
}

func TestSTV_SWF(t *testing.T) {
	p := Profile{
		{1, 2, 3},
		{1, 2, 3},
		{2, 3, 1},
		{2, 1, 3},
		{3, 1, 2},
	}

	count, err := STV_SWF(p)
	if err != nil {
		t.Error(err)
	}

	// expected := Count{1: 2, 2: 2, 3: 1}

	fmt.Println("STV_SWF:", count)
}

func TestSTV_SCF(t *testing.T) {
	p := Profile{
		{1, 2, 3},
		{1, 2, 3},
		{2, 3, 1},
		{2, 1, 3},
		{3, 1, 2},
	}

	bestAlts, err := STV_SCF(p)
	if err != nil {
		t.Error(err)
	}

	// expected := []Alternative{1}

	for alts := range bestAlts {
		fmt.Println(alts)
	}
}
