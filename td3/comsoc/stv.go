package comsoc

func STV_SWF(p Profile) (Count, error) {
	err := checkProfile(p)
	if err != nil {
		return nil, err
	}

	counts := make(Count)
	for _, voter := range p {
		if len(voter) > 0 {
			counts[voter[0]]++
		}
	}
	return counts, nil
}

func STV_SCF(p Profile) (bestAlts []Alternative, err error) {
	err = checkProfile(p)
	if err != nil {
		return nil, err
	}

	counts := make(Count) // 初始化值全为0
	totalVotes := len(p)
	threshold := totalVotes/2 + 1

	for {
		// 计数
		for _, voter := range p {
			if len(voter) > 0 {
				counts[voter[0]]++
				if counts[voter[0]] >= threshold {
					return []Alternative{voter[0]}, nil // 达到阈值，选出获胜者
				}
			}
		}

		// 查找最低票数
		var minCount = totalVotes + 1 // 初始化为一个比最大可能票数还大的值
		for _, count := range counts {
			if count >= 0 && count < minCount {
				minCount = count
			}
		}

		// 淘汰最低票数的候选人
		eliminated := make(map[Alternative]bool)
		for alt, count := range counts {
			if count == minCount {
				eliminated[alt] = true
			}
		}

		// 如果所有剩余的候选人都有相同的票数，那么它们都是胜者
		if len(eliminated) == len(counts) {
			for alt := range counts {
				bestAlts = append(bestAlts, alt)
			}
			return bestAlts, nil
		}

		// 转移被淘汰候选人的票
		for i, voter := range p {
			// 移除被淘汰的候选人
			newPrefs := []Alternative{}
			for _, alt := range voter {
				// 创建一个新的 Alternative 类型的切片 newPrefs，用于存储未被淘汰的候选人
				if !eliminated[alt] {
					newPrefs = append(newPrefs, alt)
				}
			}
			p[i] = newPrefs
		}

		// 重置计数
		for alt := range counts {
			counts[alt] = 0
		}
	}
}
