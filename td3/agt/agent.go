package agt

import (
	"fmt"
)

type AgentID int

type Alternative int

type Agent struct {
	ID    AgentID
	Name  string
	Prefs []Alternative
}

type AgentI interface {
	Equal(ag AgentI) bool
	DeepEqual(ag AgentI) bool
	Clone() AgentI
	String() string
	Prefers(a Alternative, b Alternative) bool
	Start()
}

// 比较ID
func (a Agent) Equal(ag AgentI) bool {
	// 类型断言用于检查接口变量的动态类型或将接口变量的类型从接口类型转换为具体类型。
	//ag.(Agent) 试图将接口变量 ag 的类型从 AgentI 转换为具体的 Agent 类型。
	//agt是ag转换为Agent后的值 ok表示转换是否成功
	agt, ok := ag.(Agent)
	if !ok {
		return false
	}
	return a.ID == agt.ID
}

// 比较全部参数
func (a Agent) DeepEqual(ag AgentI) bool {
	agt, ok := ag.(Agent)
	if !ok {
		return false
	}

	// 比较 ID 和 Name
	if a.ID != agt.ID || a.Name != agt.Name {
		return false
	}

	// 比较 Prefs
	if len(a.Prefs) != len(agt.Prefs) {
		return false
	}

	for i, pref := range a.Prefs {
		if pref != agt.Prefs[i] {
			return false
		}
	}

	return true
	//return reflect.DeepEqual(a, ag)
}

// 克隆一个代理
func (a Agent) Clone() AgentI {
	// a.Prefs... 语法用于将切片中的元素展开并作为变参传递给函数。
	prefs := append([]Alternative(nil), a.Prefs...)
	return Agent{ID: a.ID, Name: a.Name, Prefs: prefs}
}

// 返回代理的字符串形式？
func (a Agent) String() string {
	prefsStr := fmt.Sprintf("%v", a.Prefs) // 将 Prefs 转换为字符串
	return fmt.Sprintf("Agent %d: %s, Prefs: %s", a.ID, a.Name, prefsStr)
}

// 当前代理更倾向于哪个候选人
// 如果返回true表示倾向于a1，返回false表示倾向于a2，该如何表示中立？
func (a Agent) Prefers(a1 Alternative, a2 Alternative) bool {
	for _, alt := range a.Prefs {
		if alt == a1 {
			return true
		}
		if alt == a2 {
			return false
		}
	}
	return false
}

func (a Agent) Start() {
	// 实现决策制定或投票逻辑
	fmt.Println("Start: AgentID: ", a.ID, " Name: ", a.Name)
}
