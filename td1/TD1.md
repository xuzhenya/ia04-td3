## Pourquoi avoir ajouté un répertoire cmd ?

cmd目录常用于存放项目的可执行文件。
这是Go语言社区中的一种约定，当你的项目可能包含多个可执行程序时，将它们放在cmd目录下有助于组织代码。
Le répertoire cmd est généralement utilisé pour stocker les exécutables de votre projet.
Une convention dans la communauté du langage Go veut que lorsque votre projet peut contenir plusieurs exécutables, les placer dans le répertoire cmd permet d'organiser le code.


## Que font-elles ? Quelle est leur utilité ?

go version: 显示Go当前的版本信息。 affiche les informations sur la version actuelle de Go.

go build: 编译源代码文件。 compilez les fichiers de code source.
go build -o xxx xxx.go xxx.go
./hello进行测试

go run: 编译并运行Go程序。 compilez et exécutez des programmes Go.
go run main.go进行测试

go install: 编译并安装包或应用程序。compilez et installez un package ou une application.
如果你想将这个程序安装到Go的bin目录中，以便能够在任何位置运行它，你可以使用go install命令。

gofmt / go fmt: 格式化Go源代码。 formater le code source Go.
在编写Go代码时，格式是很重要的。运行这个命令可以自动格式化你的Go代码。
gofmt -w main.go  # 或者在 cmd/hello 目录下运行 go fmt

go doc: 显示关于Go语言包的文档。 affiche la documentation sur le package de langue Go.
如果你在代码中使用了标准库或第三方库，go doc命令能让你方便地查看库的文档。
go doc fmt.Println  # 查看fmt.Println的文档

## godoc -http=:6060
命令 `godoc -http=:6060` 用于启动 Go 的文档服务器，让您可以在本地浏览 Go 语言标准库和您自己编写的代码的文档。

- `godoc` 是 Go 的文档工具，用于生成和浏览 Go 代码的文档。
- `-http=:6060` 参数指定了文档服务器的监听地址和端口。在这个例子中，文档服务器将在本地监听端口 6060 上。

要运行这个命令，请打开终端并输入：

```shell
godoc -http=:6060
```

然后，您可以在浏览器中访问 `http://localhost:6060`，以查看 Go 语言标准库和您自己的代码的文档。您可以在这个本地文档服务器上浏览各种 Go 包的文档，查看函数、方法、常量等的说明和示例。

请确保您的 Go 工具链已正确安装，并且 `godoc` 可执行文件在您的系统中可用。通常情况下，`godoc` 应该随着 Go 的安装而自动安装。如果您遇到问题，请检查您的 Go 安装是否正确，并确保 `$GOPATH/bin` 目录在您的 `PATH` 环境变量中，以便能够运行 `godoc`。

## go install
可执行文件会被写入bin中

## main.go
在 Go 语言中，每个可执行程序（也就是包含 main 函数的程序）必须位于单独的目录下。这意味着在一个目录下只能存在一个 main 函数，因为编译器无法区分多个 main 函数应该编译成哪个可执行程序。
cmd下可以构建多个目录，每个目录之下都可以存放一个main.go