package main

import (
	"fmt"
)

func main() {
	// 创建多个通道，用于同步goroutines
	// 在第一个goroutine执行完后，会往ch1中写入true，第二个goroutine只有在从ch1中读到东西，才会继续执行
	// 当第二个goroutine执行完毕后，往ch2中写入true，这样就实现了每个goroutine按顺序执行
	ch1 := make(chan bool)
	ch2 := make(chan bool)
	ch3 := make(chan bool)
	ch4 := make(chan bool)
	ch5 := make(chan bool)
	ch6 := make(chan bool)

	sl := make([]int, 10)

	// 为Affiche_pairs启动一个goroutine
	go func() {
		Affiche_pairs()
		fmt.Println("========================================")
		ch1 <- true // 发送信号，表示这个goroutine完成了任务
	}()

	// 为Fill启动一个goroutine
	go func() {
		<-ch1 // 等待前一个goroutine完成
		Fill(sl)
		fmt.Println("Slice initial:", sl)
		fmt.Println("========================================")
		ch2 <- true
	}()

	// 为Moyenne启动一个goroutine
	go func() {
		<-ch2 // 等待前一个goroutine完成
		moyenne := Moyenne(sl)
		fmt.Println("Moyenne de sl:", moyenne)
		fmt.Println("========================================")
		ch3 <- true
	}()

	// 为ValeursCentrales启动一个goroutine
	go func() {
		<-ch3 // 等待前一个goroutine完成
		valeursCentrales := ValeursCentrales(sl)
		fmt.Println("Valeurs Centrales de sl:", valeursCentrales)
		fmt.Println("========================================")
		ch4 <- true
	}()

	// 为Plus1启动一个goroutine
	go func() {
		<-ch4 // 等待前一个goroutine完成
		Plus1(sl)
		fmt.Println("Après Plus1:", sl)
		fmt.Println("========================================")
		ch5 <- true
	}()

	// 为Compte启动一个goroutine
	go func() {
		<-ch5 // 等待前一个goroutine完成
		Compte(sl)
		fmt.Println("========================================")
		ch6 <- true
	}()

	<-ch6 // 等待最后一个goroutine完成
}
