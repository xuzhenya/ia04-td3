package main

func main() {
	go Affiche_pairs()

}

//如果你用go关键字（用于启动goroutines）来打印1至1000之间的偶数，你会看到程序的行为变得不可预测。
//这是因为main()函数和goroutine会并行运行，而main()函数可能会在goroutine完成之前结束，这样你就看不到所有输出。
//这种情况发生是因为Go程序默认在main()函数结束时就会结束所有goroutine，无论它们是否完成任务。
