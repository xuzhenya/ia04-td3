package main

import (
	"fmt"
)

// Exercice1
// Écrire un programme qui affiche les nombres pairs entre 1 et 1000 avec go
func Affiche_pairs() {
	for i := 1; i <= 1000; i++ {
		if i%2 == 0 {
			fmt.Print(i, " ")
		}
	}
	fmt.Println()
}
