package main

import "fmt"

func main() {

	dict := []string{"AGENT", "CHIEN", "COLOC", "ETANG", "ELLE", "GEANT", "NICHE", "RADAR"}

	fmt.Println(IsPalindrome("RADAR")) // true
	fmt.Println(IsPalindrome("AGENT")) // false

	fmt.Println(Palindromes(dict)) // [ "COLOC", "ELLE", "RADAR" ]

	fmt.Println(FootPrint("AGENT")) // renvoie "AEGNT"

	fmt.Println(Anagrams(dict))
	/* { AEGNT: [AGENT ETANG GEANT]
	CEHIN: [CHIEN NICHE]
	CCLOO: [COLOC]
	EELL:  [ELLE]
	AADRR: [RADAR] } */

	fmt.Println("========================================")

	dict, err := DictFromFile("dico-scrabble-fr.txt")
	if err != nil {
		fmt.Println("Error reading file:", err)
		return
	}

	fmt.Println("Longest palindrome:", FindLongestPalindrome(dict))
	fmt.Println("Anagrams of 'AGENTS':", FindAnagramsOfWord(dict, "AGENTS"))
	fmt.Println("Words with the most anagrams:", FindMostAnagrams(dict))
	fmt.Println("Palindrome with anagrams:", FindPalindromeWithAnagrams(dict))

}
