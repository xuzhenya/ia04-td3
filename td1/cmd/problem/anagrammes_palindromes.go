package main

import (
	"bufio"
	"os"
	"sort"
	"strings"
)

func IsPalindrome(word string) bool {
	n := len(word)
	for i := 0; i < n/2; i++ {
		if word[i] != word[n-i-1] {
			return false
		}
	}
	return true
}

func Palindromes(words []string) (l []string) {
	for _, word := range words {
		if IsPalindrome(word) {
			l = append(l, word)
		}
	}
	return
}

func FootPrint(s string) string {
	chars := strings.Split(s, "")
	sort.Strings(chars)
	return strings.Join(chars, "")
}

func Anagrams(words []string) map[string][]string {
	anagrams := make(map[string][]string)
	for _, word := range words {
		fp := FootPrint(word)
		anagrams[fp] = append(anagrams[fp], word)
	}
	return anagrams
}

// scanner.Err() 方法用于检查在 *Scanner 类型（通常用于从 io.Reader 读取数据）上进行扫描操作时是否发生了错误。
// 当使用 scanner.Scan() 方法进行循环读取数据时，如果在读取过程中出现错误，该错误会被 scanner.Err() 捕获。
func DictFromFile(filename string) ([]string, error) {
	file, err := os.Open(filename)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	var dict []string
	// bufio.NewScanner 是 bufio 包中的一个函数，它用于创建一个新的 Scanner 对象，该对象用于逐行读取输入数据，通常用于文件或其他输入源。
	// bufio.NewScanner 会返回一个 Scanner 对象，该对象具有方法用于逐行扫描输入数据。
	scanner := bufio.NewScanner(file)
	// 这是一个 for 循环，它会不断调用 scanner.Scan() 方法，直到 scanner.Scan() 返回 false 为止。
	// scanner.Scan() 方法的作用是从输入源（在这里是文件）读取下一行文本，并将其存储在 Scanner 对象内部，然后返回 true 表示成功读取一行，或者返回 false 表示已经没有更多行可读。
	// 如果 scanner.Scan() 返回 true，则 scanner.Text() 方法用于获取刚刚读取的文本行，并将其添加到名为 dict 的字符串切片中，实际上是通过 dict = append(dict, scanner.Text()) 来完成的。
	for scanner.Scan() {
		dict = append(dict, scanner.Text())
	}
	return dict, scanner.Err()
}

func FindLongestPalindrome(words []string) string {
	var longestPalindrome string
	for _, word := range words {
		if IsPalindrome(word) && len(word) > len(longestPalindrome) {
			longestPalindrome = word
		}
	}
	return longestPalindrome
}

func FindAnagramsOfWord(words []string, target string) []string {
	targetFp := FootPrint(target)
	var targetAnagrams []string
	for _, word := range words {
		if FootPrint(word) == targetFp {
			targetAnagrams = append(targetAnagrams, word)
		}
	}
	return targetAnagrams
}

func FindMostAnagrams(words []string) []string {
	anagrams := Anagrams(words)
	var mostAnagrams []string
	maxCount := 0
	// 在Go语言中，当遍历一个map类型的数据结构使用range关键字，你可以获得两个返回值：一个是键（key），另一个是值（value）
	for _, anagramList := range anagrams {
		if len(anagramList) > maxCount {
			mostAnagrams = anagramList
			maxCount = len(anagramList)
		}
	}
	return mostAnagrams
}

func FindPalindromeWithAnagrams(words []string) string {
	anagrams := Anagrams(words)
	for _, anagramList := range anagrams {
		for _, word := range anagramList {
			if IsPalindrome(word) {
				return word
			}
		}
	}
	return ""
}
