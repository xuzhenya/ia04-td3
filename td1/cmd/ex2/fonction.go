package main

import (
	"fmt"
	"math/rand"
	"sort"
	"time"
)

// Exercice1
// Écrire un programme qui affiche les nombres pairs entre 1 et 1000.
func Affiche_pairs() {
	for i := 2; i <= 1000; i += 2 {
		fmt.Print(i, " ")
	}
	fmt.Println()
}

// Exercice2
// Écrire une fonction Fill(sl []int) qui prend en entrée un slice d'entiers et qui le remplit avec des nombres aléatoires.
// 编写一个 Fill(sl []int) 函数，该函数将整数切片作为输入并用随机数填充它。
func Fill(sl []int) {
	rand.Seed(time.Now().UnixNano())
	for i := range sl {
		sl[i] = rand.Intn(100) // Génère un nombre aléatoire entre 0 et 99
	}
}

// Écrire une fonction Moyenne(sl []int) qui prend en entrée un slice d’entiers et qui renvoie la moyenne de ses éléments.
func Moyenne(sl []int) float64 {
	var somme int
	for _, val := range sl {
		somme += val
	}
	return float64(somme) / float64(len(sl))
}

// Écrire une fonction ValeursCentrales(sl []int) []int qui prend en entrée un slice d’entiers et qui renvoie la ou les valeurs centrales du slice.
// 编写一个函数 CentralValues(sl []int) []int ，它将整数切片作为输入并返回该切片的中心值。
func ValeursCentrales(sl []int) []int {
	sort.Ints(sl)
	n := len(sl)
	valeursCentrales := n / 2
	if n%2 == 1 {
		return []int{sl[valeursCentrales]}
	}
	return []int{sl[valeursCentrales-1], sl[valeursCentrales]}
}

// Écrire une fonction Plus1(sl []int) qui prend en entrée un slice d’entiers et qui ajoute un à chaque élément. Comment utiliser cette fonction pour tester les trois précédentes ?
// 编写一个函数 Plus1(sl []int)，它将整数切片作为输入，并对每个元素加一。 如何使用这个函数来测试前三个呢？
func Plus1(sl []int) {
	for i := range sl {
		sl[i]++
	}
}

// Écrire une fonction Compte(tab []int) qui affiche les nombres contenus dans tab.
func Compte(tab []int) {
	fmt.Print("Les nombres contenus dans sl: ")
	for _, val := range tab {
		fmt.Print(val, " ")
	}
	fmt.Println()
}
