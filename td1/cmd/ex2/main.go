package main

import (
	"fmt"
)

func main() {
	// fmt.Println("Hello World")
	Affiche_pairs()

	fmt.Println("========================================")

	sl := make([]int, 10000)
	Fill(sl)
	fmt.Println("Slice initial:", sl)

	fmt.Println("========================================")

	moyenne := Moyenne(sl)
	fmt.Println("Moyenne de sl:", moyenne)

	fmt.Println("========================================")

	valeursCentrales := ValeursCentrales(sl)
	fmt.Println("Valeurs Centrales de sl:", valeursCentrales)

	fmt.Println("========================================")

	Plus1(sl)
	fmt.Println("Après Plus1:", sl)

	fmt.Println("========================================")

	Compte(sl)

	fmt.Println("========================================")

}
