package main

import (
	"math"
)

// Exercice3

// 在Go语言中，如果你的方法返回一个结构体的值（非指针），那么你不能直接在这个返回值上调用方法或访问其字段（因为这个返回值是一个不可寻址的临时值）。

// Point2D
type Point2D struct {
	x, y float64
}

// NewPoint2D
func NewPoint2D(x, y float64) *Point2D {
	return &Point2D{x, y}
}

// GetX
func (p *Point2D) X() float64 {
	return p.x
}

// SetX
func (p *Point2D) SetX(x float64) {
	p.x = x
}

// GetY
func (p *Point2D) Y() float64 {
	return p.y
}

// SetY
func (p *Point2D) SetY(y float64) {
	p.y = y
}

// Clone copy Point2D
func (p *Point2D) Clone() *Point2D {
	return NewPoint2D(p.x, p.y)
}

// Module 计算点到原点的距离
func (p *Point2D) Module() float64 {
	return math.Sqrt(p.x*p.x + p.y*p.y)
}

// Rectangle 结构包含两个点：左上和右下
type Rectangle struct {
	topLeft, bottomRight Point2D
}

// NewRectangle
func NewRectangle(topLeft, bottomRight Point2D) *Rectangle {
	return &Rectangle{topLeft, bottomRight}
}

func (r *Rectangle) TopLeft() Point2D {
	return r.topLeft
}

func (r *Rectangle) BottomRight() Point2D {
	return r.bottomRight
}

func (r *Rectangle) SetTopLeft(newTopLeft Point2D) {
	r.topLeft = newTopLeft
}

func (r *Rectangle) SetBottomRight(newBottomRight Point2D) {
	r.bottomRight = newBottomRight
}

// Sprite 结构包含位置，hitbox, 缩放因子和位图文件名
type Sprite struct {
	position Point2D
	hitbox   Rectangle
	zoom     float64
	bitmap   string
}

// NewSprite 构造一个新的Sprite
func NewSprite(position Point2D, hitbox Rectangle, zoom float64, bitmap string) *Sprite {
	return &Sprite{position, hitbox, zoom, bitmap}
}

// Getters
func (s *Sprite) Position() Point2D {
	return s.position
}

func (s *Sprite) Hitbox() Rectangle {
	return s.hitbox
}

func (s *Sprite) Zoom() float64 {
	return s.zoom
}

func (s *Sprite) Bitmap() string {
	return s.bitmap
}

// Setters
func (s *Sprite) SetPosition(newPosition Point2D) {
	s.position = newPosition
}

func (s *Sprite) SetHitbox(newHitbox Rectangle) {
	s.hitbox = newHitbox
}

func (s *Sprite) SetZoom(newZoom float64) {
	s.zoom = newZoom
}

func (s *Sprite) SetBitmap(newBitmap string) {
	s.bitmap = newBitmap
}

// Move 移动Sprite到一个新的位置
func (s *Sprite) Move(newPosition Point2D) {
	s.position = newPosition
}

// Collision 计算两个Sprite的碰撞矩形
func isPointInRect(point Point2D, rect Rectangle) bool {
	topLeft := rect.TopLeft()
	bottomRight := rect.BottomRight()
	return point.X() >= topLeft.X() && point.X() <= bottomRight.X() && point.Y() <= topLeft.Y() && point.Y() >= bottomRight.Y()
}

func Collision(s1, s2 *Sprite) bool {
	// 先保存返回的结构体到局部变量
	position1 := s1.Position()
	hitbox1 := s1.Hitbox()
	topLeft1Local := hitbox1.TopLeft()
	bottomRight1Local := hitbox1.BottomRight()

	// 计算相对于中心点的偏移
	offsetX1 := (bottomRight1Local.X() - topLeft1Local.X()) / 2
	offsetY1 := (topLeft1Local.Y() - bottomRight1Local.Y()) / 2

	// 使用中心点和偏移来计算平移后的边界
	topLeft1 := NewPoint2D(position1.X()-offsetX1, position1.Y()+offsetY1)
	bottomRight1 := NewPoint2D(position1.X()+offsetX1, position1.Y()-offsetY1)
	topRight1 := NewPoint2D(position1.X()+offsetX1, position1.Y()+offsetY1)
	bottomLeft1 := NewPoint2D(position1.X()-offsetX1, position1.Y()-offsetY1)

	// 对于第二个Sprite做同样的事情
	position2 := s2.Position()
	hitbox2 := s2.Hitbox()
	topLeft2Local := hitbox2.TopLeft()
	bottomRight2Local := hitbox2.BottomRight()

	// 计算相对于中心点的偏移
	offsetX2 := (bottomRight2Local.X() - topLeft2Local.X()) / 2
	offsetY2 := (topLeft2Local.Y() - bottomRight2Local.Y()) / 2

	topLeft2 := NewPoint2D(position2.X()-offsetX2, position2.Y()+offsetY2)
	bottomRight2 := NewPoint2D(position2.X()+offsetX2, position2.Y()-offsetY2)
	topRight2 := NewPoint2D(position2.X()+offsetX2, position2.Y()+offsetY2)
	bottomLeft2 := NewPoint2D(position2.X()-offsetX2, position2.Y()-offsetY2)

	// 接下来做碰撞检测
	corners1 := []Point2D{*topLeft1, *bottomRight1, *topRight1, *bottomLeft1}
	corners2 := []Point2D{*topLeft2, *bottomRight2, *topRight2, *bottomLeft2}

	for _, point := range corners1 {
		if isPointInRect(point, *NewRectangle(*topLeft2, *bottomRight2)) {
			return true
		}
	}

	for _, point := range corners2 {
		if isPointInRect(point, *NewRectangle(*topLeft1, *bottomRight1)) {
			return true
		}
	}

	return false
}
