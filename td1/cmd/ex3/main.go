package main

import "fmt"

func main() {

	// 创建两个Sprite对象 hitbox的矩形是相对于position进行移动的
	sprite1 := NewSprite(*NewPoint2D(0, 0), *NewRectangle(*NewPoint2D(0, 5), *NewPoint2D(5, 0)), 1, "bitmap1.png")
	sprite2 := NewSprite(*NewPoint2D(3, 3), *NewRectangle(*NewPoint2D(3, 8), *NewPoint2D(8, 3)), 1, "bitmap2.png")
	sprite3 := NewSprite(*NewPoint2D(10, 10), *NewRectangle(*NewPoint2D(10, 15), *NewPoint2D(15, 10)), 1, "bitmap3.png")

	// 检查是否发生碰撞
	if Collision(sprite1, sprite2) {
		println("Sprite1 and Sprite2 collided!")
	} else {
		println("Sprite1 and Sprite2 did not collide.")
	}

	if Collision(sprite1, sprite3) {
		println("Sprite1 and Sprite3 collided!")
	} else {
		println("Sprite1 and Sprite3 did not collide.")
	}

	if Collision(sprite2, sprite3) {
		println("Sprite2 and Sprite3 collided!")
	} else {
		println("Sprite2 and Sprite3 did not collide.")
	}

	fmt.Println("========================================")

}
