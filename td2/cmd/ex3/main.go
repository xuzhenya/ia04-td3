package main

import (
	"fmt"
	"time"
)

func main() {
	// Utilisation de Sleep
	//for i := 5; i >= 1; i-- {
	//	fmt.Println(i)
	//	time.Sleep(time.Second)
	//}

	// Utilisation de After
	// time.After 返回一个通道，它将在指定的时间段后自动关闭。因为通道为空，所以读取数据会阻塞，因此起到了倒计时作用
	// 在1秒内，time.After不返回通道，我们读不到东西，我们被堵在了这里，直到1秒后，我可以从通道中读取，我重新进入循环，一次实现了倒计时的作用
	for i := 5; i >= 1; i-- {
		fmt.Println(i)
		<-time.After(time.Second)
	}

	// Utilisation de Tick
	// 使用了 time.NewTicker 创建一个定时器，每隔一秒钟触发一次。然后，它在循环中递减 count 变量，并在 count 达到零时停止了定时器，从而实现了一个简单的倒计时。最后，它输出 "Bonne année !" 表示倒计时结束

	// ticker.C 是一个通道（channel）的属性，用于从 time.Ticker 类型的定时器中获取触发的时间事件。具体来说，ticker.C 是一个类型为 chan time.Time 的通道。
	// for range ticker.C 的循环会一直等待，直到 ticker 触发下一个时间事件（每隔一秒钟）。每当定时器触发时，ticker.C 会发送一个 time.Time 类型的时间值到通道中，然后 for range 循环会从该通道中读取这个时间值，并执行循环内的代码块。

	ticker := time.NewTicker(time.Second)
	count := 5

	// 如果fmt.Println(count) count-- 写在了if之前，则count--之后会进入if判断然后直接break，输出"Bonne année !"
	// 所以fmt.Println(count)几乎和fmt.Println("Bonne année !")同时出现
	// 如果如果fmt.Println(count) count-- 写在了if之后，则还会进入一次倒计时，然后在下一次for循环才会判断到count == 0，然后触发break

	for range ticker.C {
		if count == 0 {
			ticker.Stop()
			break
		}
		fmt.Println(count)
		count--
	}

	fmt.Println("Bonne année !")
}
