package main

import (
	"fmt"
	"sync"
)

var n = 0

// 解决方法 mutex
var mutex = &sync.Mutex{}

func f() {
	mutex.Lock()
	n++
	mutex.Unlock()
}

func main() {
	for i := 0; i < 10000; i++ {
		go f()
	}

	fmt.Println("Appuyez sur entrée")
	fmt.Scanln()
	fmt.Println("n:", n)
}

//这个程序有两个主要问题：
//
//共享变量n的并发访问没有同步，导致数据竞态（Race Condition）。
//fmt.Scanln()用于阻塞主goroutine，以便其他goroutines有时间执行。如果你移除Scanln，主goroutine可能在其他goroutines之前结束，导致程序直接退出而没有打印任何东西。
//预测输出
//因为有数据竞态，所以这个程序的输出是不确定的。变量n可能会被多个goroutines同时访问和修改，因此最终的结果可能是任何值（通常小于10000）。
//
//如果去掉Scanln
//如果去掉Scanln，程序可能会直接退出，因为主goroutine可能在任何其他goroutine有机会执行之前就已经结束了，也就是说for循环创建了10000个goroutine，但是可能有些goroutine还没执行完，main已经结束了。
