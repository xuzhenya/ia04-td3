package main

import (
	"sort"
	"sync"
)

//性能
//通常，由于Go的运行时系统会自动进行一些优化，小数组或者简单任务的并行版本不一定会比非并行版本快。但是，如果数组非常大（几百MB或更大），并行版本通常会更快。
//
//并发的数量
//增加goroutine的数量可能会提高性能，但这并不总是如此。太多的goroutine可能会导致上下文切换的成本增加，从而降低性能。找到最优数量通常需要一些试验。

// 非并行版本
// remplit tab avec la valeur v
func Fill(tab []int, v int) {
	for i := range tab {
		tab[i] = v
	}
}

// 并行版本
// var wg sync.WaitGroup 在Go语言中是用于等待一组goroutines完成执行的方式。sync.WaitGroup 是一个计数信号量，我们可以使用它来统计正在执行的goroutines的数量。
// 下面是 sync.WaitGroup 常用的三个方法：
// Add(delta int): 增加计数器。
// Done(): 减少计数器。
// Wait(): 阻塞当前goroutine，直到计数器归零。
func FillParallel(tab []int, v int) {
	n := len(tab)
	chunkSize := n / 4 // 假设我们使用4个goroutines

	var wg sync.WaitGroup
	for i := 0; i < n; i += chunkSize {
		wg.Add(1)
		go func(start int) {
			defer wg.Done()
			end := start + chunkSize
			if end > n {
				end = n
			}
			for j := start; j < end; j++ {
				tab[j] = v
			}
		}(i)
	}
	wg.Wait()
}

// 非并行版本
func ForEach(tab []int, f func(int) int) {
	for i, v := range tab {
		tab[i] = f(v)
	}
}

// 并行版本
func ForEachParallel(tab []int, f func(int) int) {
	n := len(tab)
	var wg sync.WaitGroup
	chunkSize := n / 4

	for i := 0; i < n; i += chunkSize {
		wg.Add(1)
		go func(start int) {
			defer wg.Done()
			end := start + chunkSize
			if end > n {
				end = n
			}
			for j := start; j < end; j++ {
				tab[j] = f(tab[j])
			}
		}(i)
	}
	wg.Wait()
}

// 非并行版本
// copy le tableau src dans dest
func Copy(src []int, dest []int) {
	copy(dest, src)
}

// 并行版本
func CopyParallel(src []int, dest []int) {
	n := len(src)
	var wg sync.WaitGroup
	chunkSize := n / 4

	for i := 0; i < n; i += chunkSize {
		wg.Add(1)
		go func(start int) {
			defer wg.Done()
			end := start + chunkSize
			if end > n {
				end = n
			}
			copy(dest[start:end], src[start:end])
		}(i)
	}
	wg.Wait()
}

// 非并行版本
// trouve la valeur et le premier élément de tab vérifiant f (-1, 0 si rien n'est trouvé)
func Find(tab []int, f func(int) bool) (index int, val int) {
	for i, v := range tab {
		if f(v) {
			return i, v
		}
	}
	return -1, 0
}

// resultCh用于存储找到的元素和其索引，而doneCh用于通知所有其他goroutines停止工作。这样，一旦一个goroutine找到了匹配的元素，它会通过doneCh通知所有其他goroutines。
func FindParallel(tab []int, f func(int) bool) (index int, val int) {
	n := len(tab)
	var wg sync.WaitGroup
	chunkSize := n / 4

	resultCh := make(chan [2]int, 1) // 使用通道存储找到的元素和索引
	doneCh := make(chan bool, 1)     // 用于通知所有goroutines停止工作

	for i := 0; i < n; i += chunkSize {
		wg.Add(1)
		go func(start int) {
			defer wg.Done()
			end := start + chunkSize
			if end > n {
				end = n
			}
			for j := start; j < end; j++ {
				select {
				case <-doneCh: // 如果其他goroutine已找到匹配项，则停止工作
					return
				default:
					if f(tab[j]) {
						resultCh <- [2]int{j, tab[j]}
						doneCh <- true
						return
					}
				}
			}
		}(i)
	}

	wg.Wait()
	close(resultCh)
	close(doneCh)

	if result, ok := <-resultCh; ok {
		return result[0], result[1]
	}

	return -1, 0
}

// 非并行版本
// crée un nouveau tableau à partit de tab tel que les éléments du nouveau tableau sont composés des éléments du précédent tableau auxquels on a appliqué la fonction f
func Map(tab []int, f func(int) int) []int {
	result := make([]int, len(tab))
	for i, v := range tab {
		result[i] = f(v)
	}
	return result
}

// 并行版本
func MapParallel(tab []int, f func(int) int) []int {
	n := len(tab)
	result := make([]int, n)
	var wg sync.WaitGroup
	chunkSize := n / 4

	for i := 0; i < n; i += chunkSize {
		wg.Add(1)
		go func(start int) {
			defer wg.Done()
			end := start + chunkSize
			if end > n {
				end = n
			}
			for j := start; j < end; j++ {
				result[j] = f(tab[j])
			}
		}(i)
	}
	wg.Wait()
	return result
}

// 非并行版本
// reduit le tableau à un entier (agrégation de tous les éléments avec la fonction f)
// 这个函数接受一个整数数组、一个初始值和一个二元函数，然后使用这个二元函数来从数组中聚合一个单一的结果。
func Reduce(tab []int, init int, f func(int, int) int) int {
	result := init
	for _, v := range tab {
		result = f(result, v)
	}
	return result
}

// 并行版本
func ReduceParallel(tab []int, init int, f func(int, int) int) int {
	n := len(tab)
	chunkSize := n / 4
	results := make(chan int, 4)

	for i := 0; i < n; i += chunkSize {
		go func(start int) {
			result := init
			end := start + chunkSize
			if end > n {
				end = n
			}
			for j := start; j < end; j++ {
				result = f(result, tab[j])
			}
			results <- result
		}(i)
	}

	// Combine results
	finalResult := init
	for i := 0; i < 4; i++ {
		finalResult = f(finalResult, <-results)
	}
	return finalResult
}

// 非并行版本
// vérifie que la condition f est vérifiée sur tous les éléments du tableau tab
func Every(tab []int, f func(int) bool) bool {
	for _, v := range tab {
		if !f(v) {
			return false
		}
	}
	return true
}

// 并行版本
func EveryParallel(tab []int, f func(int) bool) bool {
	n := len(tab)
	chunkSize := n / 4
	results := make(chan bool, 4)

	for i := 0; i < n; i += chunkSize {
		go func(start int) {
			for j := start; j < start+chunkSize && j < n; j++ {
				if !f(tab[j]) {
					results <- false
					return
				}
			}
			results <- true
		}(i)
	}

	// Collect results
	for i := 0; i < 4; i++ {
		if !<-results {
			return false
		}
	}
	return true
}

// vérifie que la condition f est vérifiée sur au moins un élément du tableau tab
// 在这个版本中，我们简单地遍历整个数组，如果找到一个元素使得函数 f 返回 true，则立即返回 true。
func Any(tab []int, f func(int) bool) bool {
	for _, v := range tab {
		if f(v) {
			return true
		}
	}
	return false
}

func AnyParallel(tab []int, f func(int) bool) bool {
	n := len(tab)
	var wg sync.WaitGroup
	resultChan := make(chan bool)

	chunkSize := n / 4 // 假设我们使用 4 个 goroutines

	for i := 0; i < 4; i++ {
		wg.Add(1)
		go func(i int) {
			defer wg.Done()
			start := i * chunkSize
			end := start + chunkSize
			if end > n {
				end = n
			}
			for j := start; j < end; j++ {
				if f(tab[j]) {
					resultChan <- true
					return
				}
			}
			resultChan <- false
		}(i)
	}

	go func() {
		wg.Wait()
		close(resultChan)
	}()

	for result := range resultChan {
		if result {
			return true
		}
	}

	return false
}

func Sort(tab []int, comp func(int, int) bool) {
	// sort.Slice 的第一个参数是要排序的切片（tab）。第二个参数是一个用于比较切片元素的函数，该函数接受两个索引（i 和 j）并返回一个布尔值。这个布尔值决定了是否 i 索引处的元素应该在 j 索引处的元素之前。
	sort.Slice(tab, func(i, j int) bool { return comp(tab[i], tab[j]) })
}

func parallelSort(tab []int, comp func(int, int) bool, wg *sync.WaitGroup) {
	defer wg.Done()

	if len(tab) < 2 {
		return
	}

	pivot := tab[len(tab)/2]
	var less, equal, greater []int

	for _, x := range tab {
		if comp(x, pivot) { // x < pivot
			less = append(less, x)
		} else if comp(pivot, x) { // x > pivot
			greater = append(greater, x)
		} else { // x == pivot
			equal = append(equal, x)
		}
	}

	var wgInternal sync.WaitGroup
	wgInternal.Add(2)

	go func() {
		parallelSort(less, comp, &wgInternal)
	}()
	go func() {
		parallelSort(greater, comp, &wgInternal)
	}()

	wgInternal.Wait()

	// append(less, equal...)：这部分代码将less和equal两个切片连接在一起。... 是Go语言的语法糖，允许你将一个切片追加到另一个切片上。
	// append(append(less, equal...), greater...)：这里进一步将上一步结果和greater切片连接在一起。现在你得到了一个新的切片，包含了less、equal和greater的所有元素，它们分别是小于、等于和大于枢轴（pivot）的元素。
	// copy(tab, ...)：最后，copy函数将上述新生成的切片的元素复制回原始的tab切片。这是一个就地（in-place）操作，原始的tab切片现在被排序好的元素替换了。
	copy(tab, append(append(less, equal...), greater...))
}

// 非并行版本
func Equal(tab1 []int, tab2 []int) bool {
	if len(tab1) != len(tab2) {
		return false
	}
	for i, v := range tab1 {
		if v != tab2[i] {
			return false
		}
	}
	return true
}
