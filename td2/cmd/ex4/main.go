package main

import (
	"fmt"
)

func main() {
	// 创建空数组
	// var emptyArray [0]int
	// 创建空切片
	// emptySlice := []int{}
	// var emptySlice [0]int

	// 测试 Fill 和 FillParallel
	arr := make([]int, 10)
	Fill(arr, 5)
	fmt.Println("Fill:", arr)

	arrParallel := make([]int, 10)
	FillParallel(arrParallel, 5)
	fmt.Println("FillParallel:", arrParallel)

	// 测试 ForEach 和 ForEachParallel
	arr2 := []int{1, 2, 3, 4, 5}
	ForEach(arr2, func(x int) int {
		return x * 2
	})
	fmt.Println("ForEach:", arr2)

	arr2Parallel := []int{1, 2, 3, 4, 5}
	ForEachParallel(arr2Parallel, func(x int) int {
		return x * 2
	})
	fmt.Println("ForEachParallel:", arr2Parallel)

	// 测试 Copy 和 CopyParallel
	arr3 := make([]int, 5)
	Copy(arr2, arr3)
	fmt.Println("Copy:", arr3)

	arr3Parallel := make([]int, 5)
	CopyParallel(arr2Parallel, arr3Parallel)
	fmt.Println("CopyParallel:", arr3Parallel)

	// 测试 Find 和 FindParallel
	arr4 := make([]int, 5)
	Copy(arr2, arr4)
	index, value := Find(arr4, func(x int) bool { return x == 3 })
	if index == -1 {
		fmt.Println("Find: je ne trouve pas x = 3")
	} else {
		fmt.Println("Find: index =", index, "value =", value)
	}
	index2, value2 := Find(arr4, func(x int) bool { return x == 4 })
	if index2 == -1 {
		fmt.Println("je ne trouve pas x = 4 dans le tab")
	} else {
		fmt.Println("index =", index2, "value =", value2)
	}

	// 测试 Copy 和 CopyParallel
	arr4Parallel := make([]int, 5)
	CopyParallel(arr2Parallel, arr4Parallel)
	indexParallel, valueParallel := FindParallel(arr4, func(x int) bool { return x == 3 })
	if indexParallel == -1 {
		fmt.Println("FindParallel: je ne trouve pas x = 3")
	} else {
		fmt.Println("FindParallel: index =", indexParallel, "value =", valueParallel)
	}
	index2Parallel, value2Parallel := FindParallel(arr4, func(x int) bool { return x == 4 })
	if index2 == -1 {
		fmt.Println("Find: je ne trouve pas x = 4 dans le tab")
	} else {
		fmt.Println("FindParallel: index =", index2Parallel, "value =", value2Parallel)
	}

	// 测试 Map 和 MapParallel
	arr5 := []int{10, 20, 30, 40, 50}
	arr5_map := Map(arr5, func(x int) int { return x * 10 })
	fmt.Println("Map:", arr5_map)
	arr5Parallel := []int{10, 20, 30, 40, 50}
	arr5Parallel_map := MapParallel(arr5Parallel, func(x int) int { return x * 10 })
	fmt.Println("MapParallel:", arr5Parallel_map)

	// 测试 Reduce 和 ReduceParallel
	arr6 := []int{1, 1, 1, 1, 1}
	result := Reduce(arr6, 0, func(result, x int) int {
		return result + x
	})
	fmt.Println("Reduce:", result)
	arr6Parallel := []int{1, 1, 1, 1, 1}
	resultParallel := Reduce(arr6Parallel, 0, func(resultParallel, x int) int {
		return resultParallel + x
	})
	fmt.Println("ReduceParallel:", resultParallel)

}
