package main

import (
	"fmt"
	"time"
)

func main() {
	// compte(10)
	// go compte(10)

	// les deux goroutines s'exécutent en parallèle et que leur sortie peut être entrelacée, car elles partagent la même ressource d'écriture standard (stdout).
	// go compteMsg(100, "GoRoutine 1 ")和go compteMsg(100, "GoRoutine 2 ")将并发执行。
	// 运行结果可能因每次执行而异，因为goroutines可能会以不同的顺序并发执行。这意味着输出中的"GoRoutine 1"和"GoRoutine 2"的顺序是不可预测的。
	//go compteMsg(100, "GoRoutine 1 ")
	//go compteMsg(100, "GoRoutine 2 ")

	for i := 0; i < 10; i++ {
		start := i * 10
		end := start + 10
		msg := fmt.Sprintf("GoRoutine %d", i)
		go compteMsgFromTo(start, end, msg)
	}

	time.Sleep(10 * time.Second)
}
