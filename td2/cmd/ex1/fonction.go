package main

import "fmt"

func compte(n int) {
	for i := 0; i < n; i++ {
		fmt.Print(i, " ")
	}
}

func compteMsg(n int, msg string) {
	for i := 0; i < n; i++ {
		fmt.Printf("%s %d\n", msg, i)
	}
}

func compteMsgFromTo(start int, end int, msg string) {
	for i := start; i < end; i++ {
		fmt.Printf("%s %d\n", msg, i)
	}
}
