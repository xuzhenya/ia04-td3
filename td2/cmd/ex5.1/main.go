package main

import (
	"fmt"
	"time"
)

type Agent interface {
	Start()
}

type PingAgent struct {
	ID string
	c  chan string
}

func (p *PingAgent) Start() {
	for {
		p.c <- "Ping"
		time.Sleep(time.Second)
		fmt.Println(<-p.c)
	}
}

type PongAgent struct {
	ID string
	c  chan string
}

func (p *PongAgent) Start() {
	for {
		fmt.Println(<-p.c)
		p.c <- "Pong"
	}
}

func main() {
	ch := make(chan string)

	ping := &PingAgent{ID: "Pinger", c: ch}
	pong := &PongAgent{ID: "Ponger", c: ch}

	go ping.Start()
	go pong.Start()

	select {}
}
