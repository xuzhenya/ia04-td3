package main

import "time"

var a, b int

func f() {
	a = 1
	b = 2
}
func g() {
	print(b)
	print(a)
}
func main() {
	go f()
	time.Sleep(5 * time.Millisecond)
	g()
}
