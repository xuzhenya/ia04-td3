package main

import (
	"fmt"
	"time"
)

const PingString = "ping"
const PongString = "pong"

type Request struct {
	senderID string
	req      string
	c        chan string
}

type PingAgent struct {
	ID   string
	cin  chan string  // 等待接收消息的管道
	cout chan Request // 用于发送请求的管道
}

func NewPingAgent(id string, cout chan Request) *PingAgent {
	cin := make(chan string)
	return &PingAgent{id, cin, cout}
}

func (ag *PingAgent) Start() {
	go func() {
		for {
			ag.cout <- Request{ag.ID, PingString, ag.cin} // 通过cout向ponger发送请求，并告诉ponger用ag.cin进行回复，所有pinger共享一个ponger的c(cout)，但每个pinger有自己的cin，用于接收ponger的回复
			answer := <-ag.cin
			fmt.Printf("agent %q has received: %q\n", ag.ID, answer)
			time.Sleep(time.Second)
		}
	}()
}

type PongAgent struct {
	ID string
	c  chan Request
}

func NewPongAgent(id string, c chan Request) *PongAgent {
	return &PongAgent{id, c}
}

func (ag *PongAgent) Start() {
	go func() {
		for {
			req := <-ag.c
			fmt.Printf("agent %q has received %q from %q\n", ag.ID,
				req.req, req.senderID)
			go ag.handlePing(req)
		}
	}()
}

func (ag PongAgent) handlePing(req Request) {
	req.c <- PongString
}

func main() {
	c := make(chan Request)
	ponger := NewPongAgent("ponger", c)
	ponger.Start()
	for i := 0; i < 5; i++ {
		id := fmt.Sprintf("pinger n°%d", i)
		pinger := NewPingAgent(id, c)
		pinger.Start()
	}
	time.Sleep(time.Minute)
}
