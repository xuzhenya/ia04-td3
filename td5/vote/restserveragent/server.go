package restserveragent

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"
	"sync"
	"time"

	"gitlab.utc.fr/zhenyang/ia04/td5/comsoc"
	"gitlab.utc.fr/zhenyang/ia04/td5/types"
)

// 定义了一个 RestServerAgent 结构体，包含一个互斥锁、一个 ID、一个请求计数器和一个地址。
type RestServerAgent struct {
	sync.Mutex
	id       string
	reqCount int
	ballots  map[string]types.Ballot
	addr     string
}

func NewRestServerAgent(addr string) *RestServerAgent {
	return &RestServerAgent{
		id:      addr,
		addr:    addr,
		ballots: make(map[string]types.Ballot), // 初始化 ballots map
	}
}

// Test de la méthode 这个方法用于检查 HTTP 请求的方法是否是允许的。如果不是，它会返回一个 405 Method Not Allowed 错误。
func (rsa *RestServerAgent) checkMethod(method string, w http.ResponseWriter, r *http.Request) bool {
	if r.Method != method {
		w.WriteHeader(http.StatusMethodNotAllowed) // 405 Method Not Allowed
		fmt.Fprintf(w, "method %q not allowed", r.Method)
		return false
	}
	return true
}

func (*RestServerAgent) decodeNewBallotRequest(r *http.Request) (req types.NewBallotRequest, err error) {
	buf := new(bytes.Buffer)
	_, err = buf.ReadFrom(r.Body)
	if err != nil {
		return req, err // 读取 Body 时出错
	}
	// read from request and write the object to req sent sa parameter
	err = json.Unmarshal(buf.Bytes(), &req)
	if err != nil {
		return req, err // JSON 解码时出错
	}
	return req, nil
}

func (*RestServerAgent) decodeVoteRequest(r *http.Request) (req types.VoteRequest, err error) {
	buf := new(bytes.Buffer)
	_, err = buf.ReadFrom(r.Body)
	if err != nil {
		return req, err // 读取 Body 时出错
	}
	// read from request and write the object to req sent sa parameter
	err = json.Unmarshal(buf.Bytes(), &req)
	if err != nil {
		return req, err // JSON 解码时出错
	}
	return req, nil
}

func (*RestServerAgent) decodeResultVoteRequest(r *http.Request) (req types.ResultVoteRequest, err error) {
	buf := new(bytes.Buffer)
	_, err = buf.ReadFrom(r.Body)
	if err != nil {
		return req, err // 读取 Body 时出错
	}
	// read from request and write the object to req sent sa parameter
	err = json.Unmarshal(buf.Bytes(), &req)
	if err != nil {
		return req, err // JSON 解码时出错
	}
	return req, nil
}

func (rsa *RestServerAgent) doReqNewBallot(w http.ResponseWriter, r *http.Request) {

	rsa.Lock()
	defer rsa.Unlock()
	rsa.reqCount++

	if !rsa.checkMethod("POST", w, r) {
		return
	}

	req, err := rsa.decodeNewBallotRequest(r)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprint(w, err.Error())
		return
	}

	// 验证请求是否有效
	err = req.Validate()
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprint(w, err.Error())
		return
	}

	// 创建新的 Ballot
	ballotID := "vote_" + req.Rule // 这里生成唯一的 ID
	ballot := types.Ballot{
		ID:         ballotID,
		Rule:       req.Rule,
		Deadline:   req.Deadline,
		VoterIDs:   req.VoterIDs,
		Alts:       req.Alts,
		TieBreak:   req.TieBreak,
		Seuil:      req.Seuil,
		HasCounted: false,
	}

	// 存储新的 Ballot
	rsa.ballots[ballotID] = ballot

	// 创建响应
	resp := types.NewBallotResponse{
		BaseResponse: types.BaseResponse{
			Success: true,
			Message: "Ballot created successfully",
		},
		BallotID: ballotID,
	}

	w.WriteHeader(http.StatusCreated)
	serial, _ := json.Marshal(resp)
	w.Write(serial)
}

func (rsa *RestServerAgent) doVote(w http.ResponseWriter, r *http.Request) {
	rsa.Lock()
	defer rsa.Unlock()
	rsa.reqCount++

	if !rsa.checkMethod("POST", w, r) {
		return
	}

	// 解码投票请求
	req, err := rsa.decodeVoteRequest(r)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest) // 400 Bad Request
		fmt.Fprint(w, "Invalid vote request: ", err.Error())
		return
	}

	// 检查 ballot ID 是否有效
	ballot, exists := rsa.ballots[req.BallotID]
	if !exists {
		w.WriteHeader(http.StatusBadRequest) // 400 Bad Request
		fmt.Fprint(w, "Invalid ballot ID")
		return
	}

	// 检查投票的截止日期
	if time.Now().After(ballot.Deadline) {
		w.WriteHeader(http.StatusServiceUnavailable) // 503 Service Unavailable
		fmt.Fprint(w, "Voting deadline has passed for this ballot")
		return
	}

	// 检查选民是否已经投过票
	hasVoted := false
	for _, voteID := range ballot.VoterIDs {
		if voteID == req.AgentID {
			hasVoted = true
			break
		}
	}
	if hasVoted {
		w.WriteHeader(http.StatusForbidden) // 403 Forbidden vote already cast
		fmt.Fprint(w, "Vote already cast by this voter")
		return
	}

	// if ballot.Rule == "approval" && len(ballot.Thresholds) > 0 {
	// 	threshold := ballot.Thresholds[0]
	// 	if len(req.Prefs) < threshold {
	// 		w.WriteHeader(http.StatusBadRequest)
	// 		fmt.Fprintf(w, "Not enough approvals, %d given but %d needed", len(req.Prefs), threshold)
	// 		return
	// 	}
	// }

	// 更新投票的状态
	ballot.Votes = append(ballot.Votes, req.Prefs)             // 直接添加投票偏好
	ballot.VoterIDs = append(ballot.VoterIDs, req.AgentID)     // hotfix1015: 添加投票者 ID
	ballot.Thresholds = append(ballot.Thresholds, req.Options) // hotfix1015: 添加阈值; 记录下先，用只有approval会用上
	rsa.ballots[req.BallotID] = ballot

	// 返回成功响应
	resp := types.VoteResponse{
		BaseResponse: types.BaseResponse{
			Success: true,
			Message: "Vote accepted",
		},
	}

	w.WriteHeader(http.StatusOK) // 200 OK
	serial, _ := json.Marshal(resp)
	w.Write(serial)
}

// 这个方法返回到目前为止收到的请求总数。它用于监视和调试。
func (rsa *RestServerAgent) doReqcount(w http.ResponseWriter, r *http.Request) {

	if !rsa.checkMethod("GET", w, r) {
		return
	}

	resp := struct {
		Success bool `json:"success"`
		Count   int  `json:"count"`
	}{
		Success: true,
		Count:   rsa.reqCount,
	}

	w.WriteHeader(http.StatusOK) // 200 OK
	rsa.Lock()
	defer rsa.Unlock()
	serial, _ := json.Marshal(resp)
	w.Write(serial)
}

// 这个方法返回投票结果。它会检查投票是否已经关闭，如果没有关闭，它会返回一个425 Too Early错误；404 Not Found 错误会在投票不存在时返回。
func (rsa *RestServerAgent) doReqresult(w http.ResponseWriter, r *http.Request) {
	rsa.Lock()
	defer rsa.Unlock()
	rsa.reqCount++

	if !rsa.checkMethod("POST", w, r) {
		return
	}

	req, err := rsa.decodeResultVoteRequest(r)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprint(w, err.Error())
		return
	}

	// 检查该ballot是否存在，如果不存在，返回 404 Not Found 错误
	ballot, exists := rsa.ballots[req.BallotID]
	if !exists {
		w.WriteHeader(http.StatusNotFound) // 404 Not Found
		fmt.Fprintf(w, "Ballot with ID %s does not exist", req.BallotID)
		return
	}

	// 检查投票是否已经关闭， 如果没有关闭，返回 425 Too Early 错误
	if time.Now().Before(ballot.Deadline) {
		//w.WriteHeader(http.StatusServiceUnavailable) // 503 Service Unavailable
		w.WriteHeader(http.StatusTooEarly) // hotfix1015: 425 Too Early
		fmt.Fprintf(w, "Voting is still ongoing, results will be available after %v", ballot.Deadline)
		return
	}

	winner := ballot.Result
	// ranking := []int{2, 1, 4, 3} // 这里也只是一个示例

	resp := types.ResultVoteResponse{
		BaseResponse: types.BaseResponse{
			Success: true,
			Message: "Voting results retrieved successfully",
		},
		Winner: winner,
		// Ranking: ranking,
	}

	w.WriteHeader(http.StatusOK) // 200 OK
	serial, _ := json.Marshal(resp)
	w.Write(serial)
}

func (rsa *RestServerAgent) calculateResultCheck() {
	ticker := time.NewTicker(10 * time.Second) // 每10秒检查一次
	defer ticker.Stop()

	for { // 无限循环, 每10秒检查一次
		select {
		case <-ticker.C:
			rsa.Lock()
			for id, ballot := range rsa.ballots {
				if !ballot.HasCounted && time.Now().After(ballot.Deadline) { // 如果没有计票过，且已经过了截止日期
					var err error
					var tempResult []types.Alternative // 临时变量存储可能的多个获胜者
					switch ballot.Rule {
					case "borda":
						tempResult, err = comsoc.BordaSCF(ballot.Votes)
					case "copeland":
						tempResult, err = comsoc.CopelandSCF(ballot.Votes)
					case "majority":
						tempResult, err = comsoc.MajoritySCF(ballot.Votes)
					case "approval":
						tempResult, err = comsoc.ApprovalSCF(ballot.Votes, ballot.Thresholds)
					case "stv":
						tempResult, err = comsoc.STV_SCF(ballot.Votes)
					default:
						log.Printf("Unknown voting rule: %s\n", ballot.Rule)
						err = errors.New("unknown voting rule")
					}

					if err != nil {
						log.Printf("Error calculating result for ballot %s: %v\n", id, err)
						continue
					}

					if len(tempResult) > 1 {
						// 如果有平票的情况，使用 TieBreakFactory 函数来创建 tieBreak 函数
						tieBreak := comsoc.TieBreakFactory(ballot.TieBreak) // 确保 ballot.TieBreak 包含了预定义的候选人顺序

						winner, err := tieBreak(tempResult)
						if err != nil {
							// 处理错误，例如记录日志或者设置 ballot 的状态为错误
							log.Printf("Error breaking tie: %v\n", err)
							continue // 或者处理错误的其他方式
						}

						// 设置胜者
						ballot.Result = winner

					} else {
						ballot.Result = tempResult[0] // 只有一个获胜者 len(tempResult) == 1
					}
					ballot.HasCounted = true
					rsa.ballots[id] = ballot
					log.Printf("Ballot %s is closed and has been counted. Winner: %v\n", id, ballot.Result)
				}
			}
			rsa.Unlock()
		}
	}
}

func (rsa *RestServerAgent) Start() {
	rsa.ballots = make(map[string]types.Ballot)
	// création du multiplexer
	mux := http.NewServeMux()
	mux.HandleFunc("/vote", rsa.doVote)
	mux.HandleFunc("/count", rsa.doReqcount)
	mux.HandleFunc("/result", rsa.doReqresult)
	mux.HandleFunc("/new_ballot", rsa.doReqNewBallot)

	// création du serveur http
	s := &http.Server{
		Addr:           rsa.addr,
		Handler:        mux,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20}

	// lancement du serveur
	log.Println("Listening on", rsa.addr)
	go rsa.calculateResultCheck()
	go log.Fatal(s.ListenAndServe())
}
