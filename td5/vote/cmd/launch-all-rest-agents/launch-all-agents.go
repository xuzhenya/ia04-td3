package main

import (
	"fmt"
	"log"
	"math/rand"
	"os"
	"time"

	"gitlab.utc.fr/zhenyang/ia04/td5/types"
	"gitlab.utc.fr/zhenyang/ia04/td5/vote/restclientagent"
	"gitlab.utc.fr/zhenyang/ia04/td5/vote/restserveragent"
)

func main() {

	// 创建一个文件
	file, err := os.Create("./td5/result.txt")
	if err != nil {
		log.Fatalf("failed creating file: %s", err)
	}
	defer file.Close()

	// 设置日志输出到文件
	log.SetOutput(file)

	const nAgent = 100
	const nCandidat = 10
	const url1 = ":8080"
	const url2 = "http://localhost:8080"
	// 在这里设置approval的阀值
	const seuil = 10
	// allocates an underlying array of size n and returns a slice of length 0 and capacity n
	ballotCreationAgt := restclientagent.NewRestClientAgent("ballot_creation_agt", url2, nil, 10, 10)
	clAgts := make([]restclientagent.RestClientAgent, 0, nAgent)
	servAgt := restserveragent.NewRestServerAgent(url1)
	rand.Seed(time.Now().UnixNano())

	log.Println("démarrage du serveur...")
	go servAgt.Start()
	log.Println("démarrage du client qui génère les ballots...")
	go ballotCreationAgt.CreateBallotStart()

	time.Sleep(5 * time.Second)

	log.Println("démarrage des clients...")

	// create 100 agents and append to clAgts table
	for i := 0; i < nAgent; i++ {
		permutation := rand.Perm(nCandidat)
		prefs := make([]types.Alternative, 10)
		for i := range permutation {
			permutation[i] += 1
			prefs[i] = types.Alternative(permutation[i])
		}
		id := fmt.Sprintf("id%02d", i)
		// 根据approval的阀值，在这里给每个投票人随机生成一个1到seuil-1的数，表示投给几个候选人
		rand.Seed(time.Now().UnixNano())
		options := rand.Intn(seuil-1) + 1
		agt := restclientagent.NewRestClientAgent(id, url2, prefs, seuil, options) // hotfix1015: 还是关于approval的threshold怎么定义的问题
		clAgts = append(clAgts, *agt)
	}

	for _, agt := range clAgts {
		// attention, obligation de passer par cette lambda pour faire capturer la valeur de l'itération par la goroutine
		func(agt restclientagent.RestClientAgent) {
			go agt.Start()
		}(agt)
	}

	fmt.Scanln()
}
