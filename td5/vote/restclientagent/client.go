package restclientagent

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"math/rand"
	"net/http"
	"time"

	"gitlab.utc.fr/zhenyang/ia04/td5/types"
)

type RestClientAgent struct {
	id      string
	url     string
	prefs   []types.Alternative
	seuil   int // valabe uniquement pour ApprovalSCF (client de vote)
	options int
}

// 创建新的客户端代理
func NewRestClientAgent(id string, url string, prefs []types.Alternative, seuil int, options int) *RestClientAgent {
	return &RestClientAgent{id, url, prefs, seuil, options}
}

// 这个方法用于处理从服务器接收到的HTTP响应。它从响应体中读取JSON数据，并将其解码为types.VoteResponse结构体。该方法返回从服务器接收到的计算结果。
func (rca *RestClientAgent) treatVoteResponse(r *http.Response) string {
	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)
	var resp types.VoteResponse
	json.Unmarshal(buf.Bytes(), &resp)

	if resp.Success {
		return "Vote successful"
	} else {
		return fmt.Sprintf("Vote failed: %s", resp.Message)
	}
}

// 当向服务器申请完新的Ballot后，服务器会返回一个响应。这个方法用于处理这个响应。它从响应体中读取JSON数据，并将其[解码]为types.NewBallotResponse结构体。该方法返回从服务器接收到的计算结果。
func (rca *RestClientAgent) treatNewBallotResponse(r *http.Response) string {
	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)
	var resp types.NewBallotResponse
	json.Unmarshal(buf.Bytes(), &resp)

	if resp.Success {
		return fmt.Sprintf("New ballot created successfully, Ballot ID: %s", resp.BallotID)
	} else {
		return fmt.Sprintf("Failed to create new ballot: %s", resp.Message)
	}
}

// 解码投票结果为types.ResultVoteResponse结构体
func (rca *RestClientAgent) treatResultVoteResponse(r *http.Response) (types.Alternative, error) {
	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)
	var resp types.ResultVoteResponse
	err := json.Unmarshal(buf.Bytes(), &resp)
	if err != nil {
		return 0, err
	}

	if !resp.Success {
		return 0, fmt.Errorf(resp.Message)
	}

	return resp.Winner, nil
}

// 解码访问服务器次数结果？为types.CountResponse结构体
func (rca *RestClientAgent) treatCountResponse(r *http.Response) (int, error) {
	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)
	var resp struct {
		Count   int    `json:"count"`
		Success bool   `json:"success"`
		Message string `json:"message"`
	}
	err := json.Unmarshal(buf.Bytes(), &resp)
	if err != nil {
		return 0, err
	}

	if !resp.Success {
		return 0, fmt.Errorf("failed to retrieve count: %s", resp.Message)
	}

	return resp.Count, nil
}

func (rca *RestClientAgent) doNewBallotRequest(rule string, deadline string, voterIDs []string, alts int, tieBreak []types.Alternative) (string, error) {
	// 将 deadline 转换为 time.Time 类型
	deadlineTime, err := time.Parse(time.RFC3339, deadline)
	if err != nil {
		return "", err
	}

	// 创建一个新的投票请求
	req := types.NewBallotRequest{
		Rule:     rule,
		Deadline: deadlineTime,
		VoterIDs: voterIDs,
		Alts:     alts,
		TieBreak: tieBreak,
	}

	// 如果是approval投票，则需要设置阀值
	if rule == "approval" {
		req.Seuil = rca.seuil // 设置阈值
	}

	// 验证新的投票请求是否有效
	if err := req.Validate(); err != nil {
		return "", err
	}

	// 将请求转换为 JSON 格式
	jsonData, err := json.Marshal(req)
	if err != nil {
		return "", err
	}

	// 发送 HTTP 请求
	response, err := http.Post(rca.url+"/new_ballot", "application/json", bytes.NewBuffer(jsonData))
	if err != nil {
		return "", err
	}
	defer response.Body.Close()

	// 如果状态码不是 201 Created，则返回一个错误
	if response.StatusCode != http.StatusCreated {
		err = fmt.Errorf("[%d] %s", response.StatusCode, response.Status)
		return "", err
	}

	// 处理 HTTP 响应
	return rca.treatNewBallotResponse(response), nil
}

func (rca *RestClientAgent) doVoteRequest(ballotID string) (string, error) {
	// 创建一个投票请求
	req := types.VoteRequest{
		AgentID:  rca.id,
		BallotID: ballotID,
		Prefs:    rca.prefs,
		//Options:  rca.seuil,
	}

	if ballotID == "vote_approval" { // 判断是否为approval投票
		rand.Seed(time.Now().UnixNano())         // 设置随机数种子
		rca.options = rand.Intn(rca.seuil-1) + 1 // 随机生成 Options 值
		req.Options = rca.options                // 更新请求的 Options 字段
	}

	// 将请求转换为 JSON 格式
	jsonData, err := json.Marshal(req)
	if err != nil {
		return "", err
	}

	// 发送 HTTP 请求
	response, err := http.Post(rca.url+"/vote", "application/json", bytes.NewBuffer(jsonData))
	if err != nil {
		return "", err
	}
	defer response.Body.Close()

	// 如果状态码不是200 OK，则返回一个错误
	if response.StatusCode != http.StatusOK {
		return "", fmt.Errorf("failed to get count: %d %s", response.StatusCode, response.Status)
	}

	// 处理 HTTP 响应
	return rca.treatVoteResponse(response), nil
}

func (rca *RestClientAgent) doResultVoteRequest(ballotID string) (winner types.Alternative, err error) {
	// 发送 HTTP POST 请求来获取投票结果，注意这里改为 POST 请求和添加了请求体
	req := types.ResultVoteRequest{
		BallotID: ballotID,
	}
	jsonData, _ := json.Marshal(req)
	response, err := http.Post(rca.url+"/result", "application/json", bytes.NewBuffer(jsonData))
	// reqBody, _ := json.Marshal(map[string]string{"ballot-id": ballotID})
	// response, err := http.Post(rca.url+"/result", "application/json", bytes.NewBuffer(reqBody))
	if err != nil {
		return types.Alternative(0), err // 返回0, nil 和错误信息 ; hotfix1015: Alternative(0) instead of 0
	}
	defer response.Body.Close()

	// 如果状态码不是200 OK，则返回一个错误
	if response.StatusCode != http.StatusOK {
		return 0, fmt.Errorf("failed to get count: %d %s", response.StatusCode, response.Status)
	}

	// 处理响应并返回结果
	winner, err = rca.treatResultVoteResponse(response)
	if err != nil {
		return types.Alternative(0), err // 如果处理响应时发生错误，返回0, nil 和错误信息
	}

	return winner, nil // 返回获胜者和排名
}

func (rca *RestClientAgent) doCountRequest() (int, error) {
	// 发送 HTTP GET 请求到服务器的 /count 端点
	// response, err := http.Get(fmt.Sprintf("%s/count", rca.url))
	response, err := http.Get(rca.url + "/count")
	if err != nil {
		return 0, err // 如果请求失败，返回 0 和错误
	}
	defer response.Body.Close()

	// 如果 HTTP 状态码不是 200 OK，返回一个错误
	if response.StatusCode != http.StatusOK {
		return 0, fmt.Errorf("failed to get count: %d %s", response.StatusCode, response.Status)
	}

	// 处理响应并返回计数
	return rca.treatCountResponse(response)
}

func (rca *RestClientAgent) CreateBallotStart() {
	log.Println("Creating different ballots...")

	// 定义一个 ballots 数组，包含各种投票规则
	ballots := []string{"borda", "majority", "copeland", "approval", "stv"}

	// 对每种投票规则，创建一个新的投票
	for _, ballot := range ballots {
		// 此处的 nil 可能需要替换为实际的投票者ID列表
		// 时间也应该根据实际情况进行调整

		// 添加一个 tieBreak 列表作为决胜规则
		tieBreak := []types.Alternative{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}

		// 修改这里以传递 tieBreak 参数
		response, err := rca.doNewBallotRequest(ballot, time.Now().Add(10*time.Second).Format(time.RFC3339), nil, 100, tieBreak)
		if err != nil {
			log.Fatalf("[%s] Error: %s\n", rca.id, err.Error())
			continue
		}
		log.Printf("[POST][%s] Request to create ballot, id = %v, %s\n", rca.id, ballot, response)
	}
}

func generateRandomPrefs() []types.Alternative {
	candidates := []types.Alternative{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}                                               // 这里我们假设候选人的标识符是从 0 到 9
	rand.Seed(time.Now().UnixNano())                                                                              // 初始化随机数生成器种子
	rand.Shuffle(len(candidates), func(i, j int) { candidates[i], candidates[j] = candidates[j], candidates[i] }) // 随机打乱候选人顺序
	return candidates
}

func (rca *RestClientAgent) Start() {
	log.Printf("démarrage de %s", rca.id)

	ballots := []string{"vote_borda", "vote_majority", "vote_approval", "vote_stv", "vote_copeland"}

	for _, ballot := range ballots {
		// rca.prefs = generateRandomPrefs() // 这里添加一个函数来生成随机的投票偏好
		res, err := rca.doVoteRequest(ballot)
		if err != nil {
			log.Fatal(rca.id, "error:", err.Error())
		} else {
			if ballot == "vote_approval" {
				log.Printf("[POST][%s] voting to %s, preferences = %v, %s\n", rca.id, ballot, rca.prefs[:rca.options], res)
			} else {
				log.Printf("[POST][%s] voting to %s, preferences = %v, %s\n", rca.id, ballot, rca.prefs, res)
			}
		}
	}

	time.Sleep(10 * time.Second)

	for _, ballot := range ballots {
		res, err := rca.doResultVoteRequest(ballot)
		if err != nil {
			log.Fatal(rca.id, "error:", err.Error())
		} else {
			log.Printf("[GET][%s] requesting result of election [%s], Alternative elected = %v\n", rca.id, ballot, res)
		}
	}

	time.Sleep(10 * time.Second)

	res3, err3 := rca.doCountRequest()
	if err3 != nil {
		log.Fatal(rca.id, "error:", err3.Error())
	} else {
		log.Printf("[GET][%s] requesting server request count, count = %v\n", rca.id, res3)
	}
}
