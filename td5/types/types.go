package types

import (
	"errors"
	"time"
)

type Alternative int
type Profile [][]Alternative
type Count map[Alternative]int

type Ballot struct {
	ID         string          // 选票的唯一ID
	Rule       string          // 投票规则
	Deadline   time.Time       // 投票的截止时间
	Votes      [][]Alternative // 所有的投票
	HasCounted bool            // hotfix1015: 意味着是否已经计票。当过了ddl之后，会在计算完ballot结果之后设置为true
	VoterIDs   []string        // hotfix1015: 已经投票的投票者ID列表
	Alts       int             // 选项数量
	TieBreak   []Alternative   // 平票时的决胜规则
	Result     Alternative     // 投票的结果
	Thresholds []int           // 在Approval中，每个选民的投票数量
	Seuil      int             `json:"seuil,omitempty"` // 阀值
}

// VoteRequest 代表一个投票请求
type VoteRequest struct {
	AgentID  string        `json:"agent-id"`  // 投票者ID
	BallotID string        `json:"ballot-id"` // 选票ID
	Prefs    []Alternative `json:"prefs"`     // 投票者的偏好
	Options  int           `json:"options"`   // 其他选项，例如approval; 选民的有效投票数（只在Approval投票法中使用）
}

// NewBallotRequest 代表创建新选票的请求
type NewBallotRequest struct {
	Rule     string        `json:"rule"`            // 投票规则，例如"majority", "borda"等
	Deadline time.Time     `json:"deadline"`        // 投票截止日期 format RFC 3339 "2023-10-09T23:05:08+02:00"
	VoterIDs []string      `json:"voter-ids"`       // 投票者ID列表 ["ag_id1", "ag_id2", "ag_id3"]
	Alts     int           `json:"#alts"`           // 选项数量 12
	TieBreak []Alternative `json:"tie-break"`       // 平票时的决胜规则 [4, 2, 3, 5, 9, 8, 7, 1, 6, 11, 12, 10]
	Seuil    int           `json:"seuil,omitempty"` // 阀值，用于approval
}

// ResultVoteRequest 代表一个获取投票结果的请求
type ResultVoteRequest struct {
	BallotID string `json:"ballot-id"` // 选票ID
}

// BaseResponse 是所有响应类型的基础结构体
type BaseResponse struct {
	Success bool   `json:"success"` // 表示请求是否成功
	Message string `json:"message"` // 可能包含的错误或成功消息
}

// NewBallotResponse 是 /new_ballot 请求的响应
type NewBallotResponse struct {
	BaseResponse        // 嵌入基础响应结构体
	BallotID     string `json:"ballot_id,omitempty"` // 新创建的选票ID
}

// VoteResponse 是 /vote 请求的响应
type VoteResponse struct {
	BaseResponse
}

// ResultVoteResponse 是 /result 请求的响应
type ResultVoteResponse struct {
	BaseResponse
	Winner Alternative `json:"winner"`
	// Ranking []int       `json:"ranking,omitempty"` // 这个字段是可选
}

// Validate 验证 NewBallotRequest 是否有效
func (req *NewBallotRequest) Validate() error {
	if req.Rule == "" {
		return errors.New("missing voting rule")
	}
	if req.Deadline.IsZero() {
		return errors.New("missing deadline")
	}
	if req.Alts <= 0 {
		return errors.New("invalid number of alternatives")
	}
	//if len(req.TieBreak) != req.Alts {
	//	return fmt.Errorf("tie break length (%d) does not match number of alternatives (%d)", len(req.TieBreak), req.Alts)
	//}
	return nil
}

//### Alternatives（选项）
//- 选项（比如不同的候选人或方案）是用整数来表示的。例如，1 可能表示 "候选人 A"，2 可能表示 "候选人 B"，等等。
//
//### Profile（选民偏好）
//
//- 选民偏好（Profile）是一个二维数组。每个选民都有一个数组，数组中的整数按照该选民的偏好进行排序。
//- `profile[12]` 表示第 12 个选民的偏好列表。
//- 在 `profile[12]` 的数组里，第一个元素（即 `profile[12][0]`）是该选民最喜欢的选项。数组的其他元素按照选民的偏好从高到低进行排序。
//
//### Count（计数或得分）
//
//- 投票方法返回一个 `Count` 类型，这实际上是一个映射（map），它将每个选项映射到一个整数。
//- 这个整数表示该选项获得的点数或票数。这个数值越高，表示该选项在所有选民中更受欢迎。
//
//举个例子：
//假设有三个选项：1、2 和 3。
//
//- 选民 A 的偏好是 [3, 1, 2]。
//- 选民 B 的偏好是 [2, 1, 3]。
//- 选民 C 的偏好是 [1, 2, 3]。
//
//那么，Profile 可能看起来像这样：`[[3, 1, 2], [2, 1, 3], [1, 2, 3]]`。
//
//根据某个投票规则（例如多数规则、Borda 计数等），每个选项最终会获得一个得分，这将存储在一个 `Count` 类型的变量中。例如，根据某种计算，选项 1 获得了 10 分，选项 2 获得了 8 分，选项 3 获得了 6 分，那么 `Count` 可能看起来像这样：`{1: 10, 2: 8, 3: 6}`。
