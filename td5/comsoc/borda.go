package comsoc

import (
	"gitlab.utc.fr/zhenyang/ia04/td5/types"
)

// Borda
func BordaSWF(p types.Profile) (types.Count, error) {
	err := checkProfile(p)
	if err != nil {
		return nil, err
	}

	count := make(types.Count)
	for _, voter := range p {
		for i, alt := range voter {
			count[alt] += len(voter) - i - 1 // 得分基于选项在投票者偏好中的排名 假设投票为 C B A D，则B的得分为2
		}
	}

	return count, nil
}

func BordaSCF(p types.Profile) ([]types.Alternative, error) {
	count, err := BordaSWF(p)
	if err != nil {
		return nil, err
	}

	max := -1 // 初始化最大得分为负值，以确保能够找到最大得分的选项。
	var bestAlts []types.Alternative
	for alt, score := range count {
		if score > max {
			max = score
			bestAlts = []types.Alternative{alt}
		} else if score == max {
			bestAlts = append(bestAlts, alt)
		}
	}

	//if len(bestAlts) > 1 {
	//	return bestAlts, errors.New("Vote à égalité, nécessite une méthode de séparation pour déterminer le gagnant")
	//}

	return bestAlts, nil
}
