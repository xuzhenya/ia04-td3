package comsoc

import (
	"errors"
	"fmt"
	"gitlab.utc.fr/zhenyang/ia04/td5/types"
)

func TieBreakFactory(orderedAlts []types.Alternative) func([]types.Alternative) (types.Alternative, error) {
	return func(candidates []types.Alternative) (types.Alternative, error) {
		if len(candidates) == 0 {
			return 0, errors.New("La liste des candidats est vide")
		}

		// 用map来查找候选人在预定义顺序中的位置
		ranking := make(map[types.Alternative]int)
		for i, alt := range orderedAlts {
			ranking[alt] = i
		}

		// 在候选人中查找排名最高的候选人
		winner := candidates[0]
		minRank := ranking[winner]
		for _, candidate := range candidates {
			if rank, found := ranking[candidate]; found {
				if rank < minRank {
					winner = candidate
					minRank = rank
				}
			} else {
				return 0, fmt.Errorf("Le candidat %v n'est pas dans la liste de séquence prédéfinie", candidate)
			}
		}

		return winner, nil
	}
}
