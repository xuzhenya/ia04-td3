package comsoc

import (
	"errors"
	"fmt"
	"gitlab.utc.fr/zhenyang/ia04/td5/types"
)

func CondorcetWinner(p types.Profile) (bestAlts []types.Alternative, err error) {
	err = checkProfile(p)
	if err != nil {
		return nil, err
	}

	win_time := make(map[types.Alternative]int) // Number of wins for each alternative

	for i := 1; i <= len(p[0])-1; i++ {
		for j := i + 1; j <= len(p[0]); j++ {
			count_i, count_j := 0, 0
			// 遍历Profile中每一个individu的preference，进行计票
			for _, prefs := range p {
				if isPref(types.Alternative(i), types.Alternative(j), prefs) {
					count_i++
				} else {
					count_j++
				}
			}
			// 计票结果：{i,j}, 赢的alt它的win次数+1
			if count_i > count_j {
				win_time[types.Alternative(i)]++
			} else {
				win_time[types.Alternative(j)]++
			}
		}
	}

	fmt.Println(win_time)

	// 寻找Condorcet获胜者，他的win_time[Alternative(i)]应为总参选人数-1
	for key, value := range win_time {
		if value == len(p[0])-1 {
			bestAlts = append(bestAlts, types.Alternative(key))
			return bestAlts, nil
		}
	}

	return nil, errors.New("Pas de vainqueur Condorcet")
}
