package comsoc

import (
	"errors"
	"gitlab.utc.fr/zhenyang/ia04/td5/types"
)

func STV_SWF(p types.Profile) (types.Count, error) {
	err := checkProfile(p)
	if err != nil {
		return nil, err
	}

	counts := make(types.Count)
	for _, voter := range p {
		if len(voter) > 0 {
			counts[voter[0]]++
		}
	}
	return counts, nil
}

func STV_SCF(p types.Profile) (bestAlts []types.Alternative, err error) {
	err = checkProfile(p)
	if err != nil {
		return nil, err
	}

	counts := make(types.Count) // 初始化值全为0
	totalVotes := len(p)
	threshold := totalVotes/2 + 1
	maxIterations := len(p[0]) - 1 // 假设每个人都投了完整的票，所以候选人的总数是 p[0] 的长度

	for iteration := 0; iteration < maxIterations; iteration++ {
		// 计数
		for _, voter := range p {
			if len(voter) > 0 {
				counts[voter[0]]++
				if counts[voter[0]] >= threshold {
					return []types.Alternative{voter[0]}, nil // 达到阈值，选出获胜者
				}
			}
		}

		// 查找最低票数
		var minCount = totalVotes + 1 // 初始化为一个比最大可能票数还大的值
		for _, count := range counts {
			if count >= 0 && count < minCount {
				minCount = count
			}
		}

		// 淘汰最低票数的候选人
		eliminated := make(map[types.Alternative]bool)
		for alt, count := range counts {
			if count == minCount {
				eliminated[alt] = true
			}
		}

		// 如果所有剩余的候选人都有相同的票数，那么它们都是胜者
		if len(eliminated) == len(counts) || iteration == maxIterations-1 {
			// 在最大迭代次数或所有候选人都有相同票数的情况下，选择票数最多的候选人
			maxCount := 0
			for _, count := range counts {
				if count > maxCount {
					maxCount = count
				}
			}

			for alt, count := range counts {
				if count == maxCount {
					bestAlts = append(bestAlts, alt)
				}
			}
			return bestAlts, nil
		}

		// 转移被淘汰候选人的票
		for i, voter := range p {
			// 移除被淘汰的候选人
			newPrefs := []types.Alternative{}
			for _, alt := range voter {
				if !eliminated[alt] {
					newPrefs = append(newPrefs, alt)
				}
			}
			p[i] = newPrefs
		}

		// 重置计数
		for alt := range counts {
			counts[alt] = 0
		}
	}

	// 如果代码执行到这里，说明没有胜者被选出，可以添加一些错误处理或者默认返回值
	return nil, errors.New("no winner selected after maximum iterations")
}
