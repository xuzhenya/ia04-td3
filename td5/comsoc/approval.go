package comsoc

import (
	"errors"
	"fmt"
	"gitlab.utc.fr/zhenyang/ia04/td5/types"
)

// Approval
// 阈值是一个数组，是因为每个选民可以有自己的阈值。也就是说，不同的选民可能会有不同的标准或者数量的候选人被认可。通过为每个选民分配一个特定的阈值，可以使得投票系统更为灵活和个性化。
//
// 举个例子，考虑三个选民和四个候选人。每个选民可能根据自己的判断和标准，认可不同数量的候选人。例如：
// 选民1可能只认可他的第一选择
// 选民2可能认可他的前两个选择
// 选民3可能认可所有的候选人
// 这样，每个选民就可以有他们自己的阈值，例如 [1, 2, 4]。这个阈值数组意味着：
//
// 选民1的阈值是1，所以他只为他的第一选择投票
// 选民2的阈值是2，所以他为他的前两个选择投票
// 选民3的阈值是4，所以他为所有候选人投票
func ApprovalSWF(p types.Profile, thresholds []int) (types.Count, error) {
	if len(p) != len(thresholds) {
		return nil, errors.New("Le nombre d'électeurs ne correspond pas à la longueur du tableau de seuils")
	}

	err := checkProfile(p)
	if err != nil {
		return nil, err
	}

	count := make(types.Count)
	for i, voter := range p {
		threshold := thresholds[i]
		if threshold < 0 || threshold >= len(voter) {
			return nil, fmt.Errorf("threshold %d pour le voter %d : mauvaise longueur ", threshold, i)
		}

		for j, alt := range voter {
			if j <= threshold {
				count[alt]++
			}
		}
	}

	return count, nil
}

func ApprovalSCF(p types.Profile, thresholds []int) ([]types.Alternative, error) {
	count, err := ApprovalSWF(p, thresholds)
	if err != nil {
		return nil, err
	}

	max := 0
	var bestAlts []types.Alternative
	for alt, votes := range count {
		if votes > max {
			max = votes
			bestAlts = []types.Alternative{alt}
		} else if votes == max {
			bestAlts = append(bestAlts, alt)
		}
	}

	//if len(bestAlts) > 1 {
	//	return bestAlts, errors.New("Vote à égalité, nécessite une méthode de séparation pour déterminer le gagnant")
	//}

	return bestAlts, nil
}
