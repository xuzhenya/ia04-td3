### server端

## 完成
func NewRestServerAgent(addr string) *RestServerAgent
构造一个新的投票服务器代理。

## 完成
func (rsa *RestServerAgent) checkMethod(method string, w http.ResponseWriter, r *http.Request) bool
检查 HTTP 请求的方法是否被允许。

## 完成
func (*RestServerAgent) decodeVoteRequest(r *http.Request) (req types.VoteRequest, err error)
解码Vote的Request请求。

## 完成
func (*RestServerAgent) decodeResultVoteRequest(r *http.Request) (req types.ResultVoteRequest, err error)
解码Result的Request请求。

## 完成
func (*RestServerAgent) decodeNewBallotRequest(r *http.Request) (req types.NewBallotRequest, err error)
解码New Ballot的Request请求。

## 完成
func (rsa *RestServerAgent) doVote(w http.ResponseWriter, r *http.Request)
处理vote请求。

## 完成
func (rsa *RestServerAgent) doReqNewBallot(w http.ResponseWriter, r *http.Request)
处理new ballot请求。

## 完成
func (rsa *RestServerAgent) doReqresult(w http.ResponseWriter, r *http.Request)
处理result请求，返回投票结果

## 完成
func (rsa *RestServerAgent) doReqcount(w http.ResponseWriter, r *http.Request)
处理收到的请求总数

## 完成
func (rsa *RestServerAgent) calculateResultCheck(s *http.Server)
检查是否到达投票截止时间，如果是，则计算投票结果。

## 完成
func (rca *RestClientAgent) CreateBallotStart()
用于初始化创建一些投票方法。

## 完成
Start()
启动 HTTP 服务器以及投票结果计算线程。





### client端

## 完成
func NewRestClientAgent(id string, url string, prefs []types.Alternative, seuil int) *RestClientAgent
构造一个新的投票客户端代理。

## 完成
func (rca *RestClientAgent) treatVoteResponse(r *http.Response) string
处理服务器端返回的Vote请求的响应。

## 完成
func (rca *RestClientAgent) treatNewBallotResponse(r *http.Response) string
处理服务器端返回的New Ballot请求的响应。

## 完成
func (rca *RestClientAgent) treatResultVoteResponse(r *http.Response) (types.Alternative, err)
处理服务器端返回的Result请求的响应。

## 完成
func (rca *RestClientAgent) treatCountResponse(r *http.Response) int
处理服务器端返回的Count请求的响应。

## 完成
func (rca *RestClientAgent) doNewBallotRequest(rule string, deadline string, voterids []string, alts int) (res string, err error)
用于发送请求以创建新的投票。

## 完成
func (rca *RestClientAgent) doVoteRequest(ballotId string) (res string, err error)
允许客户端代理进行投票。

## 完成
func (rca *RestClientAgent) doResultVoteRequest(ballotId string) (res types.Alternative, err error)
用于获取特定投票的结果。

## 完成
func (rca *RestClientAgent) doCountRequest() (res int, err error)
返回服务器处理的请求总数。

## 完成
Start()

## doCount返回
一共有三个函数会调用一次增加一次访问服务器次数
doReqNewBallot
doReqresult
doVote

doReqNewBallot : 5种ballot x1

下边两个都有for _, ballot := range ballots的循环
doReqresult: 100个选民 x 5种ballot x1
doVote：100个选民 x 5种ballot x1

count总数 = 1005

## 问题遗留
3. readme文档编写
5. 注释中文修改
6. .exe文件生成
